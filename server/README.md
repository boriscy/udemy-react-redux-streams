### Installed knex for database migrations

To create a migration

> npx knex migrate:make migration_create_table

To run migrations

> npx knex migrate:latest

To create a seed

> npx knex seed:make 01_table_name
