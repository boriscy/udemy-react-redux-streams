DROP SCHEMA IF EXISTS app_public;
CREATE SCHEMA app_public;
drop schema if EXISTS app_private;
create schema app_private;

CREATE EXTENSION citext;
create extension if not exists "pgcrypto";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
--create extension if not exists "uuid-ossp";
--id uuid primary key default uuid_generate_v1mc(),

alter default privileges revoke all on sequences from public;
alter default privileges revoke all on functions from public;
revoke all on schema public from public;


create function app_private.tg__timestamps() returns trigger as $$
begin
  NEW.created_at = (case when TG_OP = 'INSERT' then NOW() else OLD.created_at end);
  NEW.updated_at = (case when TG_OP = 'UPDATE' and OLD.updated_at >= NOW() then OLD.updated_at + interval '1 millisecond' else NOW() end);
  return NEW;
end;
$$ language plpgsql volatile set search_path to pg_catalog, public, pg_temp;

create table app_public.users (
  id uuid primary key default gen_random_uuid(),
  email citext not null unique check (email ~* '^.+@.+\..+$'),
  full_name text,
  created_at timestamptz default now(),
  updated_at timestamptz default now()
);

create table app_private.users_account (
  user_id uuid primary key references app_public.users(id) on delete cascade,
  password_hash text not null
);

create unique index on app_private.users_account (user_id);

create trigger _100_timestamps
  before insert or update on app_public.users
  for each row
  execute procedure app_private.tg__timestamps();

CREATE TABLE app_public.streams (
  id uuid primary key default gen_random_uuid(),
  title text not null,
  description text not null,
  user_id uuid references app_public.users(id) on delete cascade,
  created_at timestamptz not null default now(),
  updated_at timestamptz not null default now()
);

create index on app_public.streams(user_id);

create trigger _100_timestamps
  before insert or update on app_public.streams
  for each row
  execute procedure app_private.tg__timestamps();

alter table app_public.users
  alter column created_at set not null,
  alter column created_at set default now(),
  alter column updated_at set not null,
  alter column updated_at set default now();
