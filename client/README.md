This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Install graphql libraries

> yarn add apollo-client apollo-cache-inmemory apollo-link-http @apollo/react-components @apollo/react-hoc @apollo/react-hooks graphql graphql-tag

In order to use GraphQl queries and mutations as a hook we need to implement ‘graphql-code-generator’

> yarn add -D @graphql-codegen/cli

Set up the codegen configuration by executing the following command:

> yarn graphql-codegen init

and set this options
https://medium.com/make-it-heady/part-2-building-full-stack-web-app-with-postgraphile-and-react-client-side-1c5085c5a182

1. The application built with **React**.

2. The schema is located at **http://localhost:8080/graphql**

3. Set your operations and fragments location to **./src/components/\*_/_.ts** so that it will search all our TypeScript files for query declarations.

4.Use the default plugins “TypeScript”, “TypeScript Operations”, “TypeScript React Apollo.”

5.Update the generated destination to src/generated/graphql.tsx (.tsx is required by the react-apollo plugin).

6. Do not generate an introspection file.

7. Use the default codegen.yml file.

8. Make your run script codegen.

> npm run codegen

Install plugin

> yarn add @graphql-codegen/typescript-operations

and start server dir

> yarn start

and then run in client

> yarn codegen
