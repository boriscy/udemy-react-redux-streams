import { SIGN_OUT, SIGN_IN, AuthMessage } from "actions/types"

interface ActionState {
  type: string
  payload: AuthMessage
}

const initialState = {
  isSignedIn: null,
  userId: null,
  email: null,
}

const authReducer = (state = initialState, action: ActionState) => {
  switch (action.type) {
    case SIGN_IN:
      const { userId, email, name } = action.payload
      return { ...state, isSignedIn: true, userId, email, name }
    case SIGN_OUT:
      return {
        ...state,
        isSignedIn: false,
        userId: null,
        email: null,
        name: null,
      }
  }
  return state
}

export default authReducer
