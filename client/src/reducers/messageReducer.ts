import { ADD_SNACK, REMOVE_SNACK } from "actions/types"

const initialState = {
  isOpen: false,
  text: "Hello",
  status: "success",
  position: { vertical: "top", horizontal: "center" },
  duration: 3000,
}

export default (state = initialState, action: any) => {
  switch (action.type) {
    case ADD_SNACK:
      return {
        ...state,
        isOpen: true,
        text: action.payload.text,
        status: action.payload.status,
      }
    case REMOVE_SNACK:
      return {
        ...state,
        isOpen: false,
      }
    default:
      return state
  }
}
