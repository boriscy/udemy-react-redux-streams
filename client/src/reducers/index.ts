import { combineReducers } from "redux"
import authReducer from "./authReducer"
import streamReducer from "./streamReducer"
import messageReducer from "./messageReducer"

export default combineReducers({
  auth: authReducer,
  stream: streamReducer,
  message: messageReducer,
})
