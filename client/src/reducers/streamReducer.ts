import { StreamMessage, FETCH_STREAMS } from "actions/types"
const initialState: StreamMessage[] = []

export default (state = initialState, action: any) => {
  switch (action.type) {
    case FETCH_STREAMS:
      return action.payload
    default:
      return state
  }
}
