import React, { Component } from "react"
import { connect } from "react-redux"
import Button from "components/bulma/Button"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import { signIn, signOut } from "actions"
import "actions/gtypes.d"

interface Props {
  auth: { isSignedIn: Boolean; email: string; userId: string }
  signIn: Function
  signOut: Function
}

class GoogleAuth extends Component<Props> {
  auth: any
  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      window.gapi.client
        .init({
          clientId:
            "815666520496-v0ccddd2fod61ak3607oouuu6thmi21h.apps.googleusercontent.com",
          scope: "email",
        })
        .then(() => {
          this.auth = window.gapi.auth2.getAuthInstance()
          if (this.auth.isSignedIn.get()) {
            this.props.signIn(this.getUserPropsObject())
          } else {
            this.props.signOut()
          }
        })
    })
  }

  getUserPropsObject() {
    const user = this.auth.currentUser.get()
    const profile = user.getBasicProfile()
    const email = profile.getEmail()
    const name = profile.getName()
    const userId = user.getId()
    return { email, userId, name }
  }

  async signIn() {
    try {
      await this.auth.signIn()
      const userProps = this.getUserPropsObject()
      this.props.signIn(userProps)
      //this.props.signInFn(userProps)
    } catch (err) {
      console.error("ERROR in SingIn")
    }
  }

  async signOut() {
    try {
      await this.auth.signOut()
      this.props.signOut()
    } catch (err) {
      console.error("ERROR in SingOut")
    }
  }

  render() {
    if (this.props.auth.isSignedIn) {
      return (
        <Button onClick={() => this.signOut()} title="Sign out with Google">
          <i className="google icon" /> Sign out
        </Button>
      )
    } else {
      return (
        <Button onClick={() => this.signIn()} className="primary">
          Sing in with Google
        </Button>
      )
    }
  }
}

const mapStateToProps = (state: any) => ({
  auth: state.auth,
})

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth)
