import React from "react"

interface ButtonProps {
  children: React.ReactNode
  onClick?: Function
  className?: string
  disabled?: boolean
  title?: string
}

const Button = (props: ButtonProps) => {
  const { children, ...btnProps } = props
  return <button>{children}</button>
}

export default Button
