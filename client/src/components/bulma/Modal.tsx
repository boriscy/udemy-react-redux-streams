import React from "react"

interface ModalProps {
  children: React.ReactElement | string | null
  onClick?: Function
  className?: string
  disabled?: boolean
  visible: boolean
  title?: string
}

const Modal = (props: ModalProps) => {
  const isVisible = props.visible ? "" : ""

  return <div className="">{props.children}</div>
}

export default Modal
