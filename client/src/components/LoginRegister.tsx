import React, { useState, Component } from "react"
import Modal from "components/bulma/Modal"
import GoogleAuth from "./GoogleAuth"

class LoginRegister extends Component {
  //const [open, setOpen] = useState(false)
  //const [tab, setTab] = useState("login")
  state = { visible: false }

  handleClose = () => {
    //setOpen(false)
  }

  handleOpen = (tab: string) => {
    this.setState({ visible: true })
    //setOpen(true)
    //setTab(tab)
  }

  registerFn = (userProps: any) => {
    console.log("register", userProps)
  }

  render() {
    return (
      <div style={{ display: "inline-block" }}>
        <div>
          <a href="#" onClick={() => this.handleOpen("login")}>
            Sign in
          </a>
          &nbsp;
          <a href="#" onClick={() => this.handleOpen("register")}>
            Register
          </a>
        </div>
        <Modal visible={this.state.visible} title="Login">
          <GoogleAuth />
        </Modal>
      </div>
    )
  }
}

export default LoginRegister
