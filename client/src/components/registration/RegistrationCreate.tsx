import React from "react"
import { useForm, Controller } from "react-hook-form"
import * as yup from "yup"

const defaultValues = {
  email: "",
}
const validationSchema = yup.object().shape({})

const Register = (props: any) => {
  const { control, handleSubmit, errors, register, reset } = useForm({
    defaultValues,
    validateCriteriaMode: "all",
    validationSchema,
  })

  return (
    <div>
      <form>
        <div className="field">
          <div className="error">{errors.email && errors.email.message}</div>
        </div>
      </form>
    </div>
  )
}
