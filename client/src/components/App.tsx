import React from "react"
import { connect } from "react-redux"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { library } from "@fortawesome/fontawesome-svg-core"
import { fab } from "@fortawesome/free-brands-svg-icons"
import { ApolloProvider } from "@apollo/react-hooks"
import client from "ApolloClient"

import Header from "components/Header"
import StreamCreate from "components/streams/StreamCreate"
import StreamEdit from "components/streams/StreamEdit"
import StreamDelete from "components/streams/StreamDelete"
import StreamShow from "components/streams/StreamShow"
import StreamList from "components/streams/StreamList"
import "../../node_modules/bulma/css/bulma.css"
import "css/base.scss"

library.add(fab)

const App = (props: any) => {
  const { isOpen, text, status, position, duration } = props.message
  //const msg = message.success(text, duration / 1000)

  return (
    <ApolloProvider client={client}>
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact component={StreamList} />
          <Route path="/streams/new" component={StreamCreate} />
          <Route path="/streams/edit" component={StreamEdit} />
          <Route path="/streams/show" component={StreamShow} />
          <Route path="/streams/delete" component={StreamDelete} />
        </Switch>
      </Router>
    </ApolloProvider>
  )
}

const mapStateToProps = (state: any) => {
  return { message: state.message }
}

export default connect(mapStateToProps)(App)
