import React from "react"
import { connect } from "react-redux"
import { useGetStreamsQuery } from "generated/graphql"
import { FETCH_STREAMS } from "actions/types"

interface StreamMessage {
  id: number
  title: string
  description: string
  createdAt: String
}

const StreamList = (props: any) => {
  const { data, loading, error } = useGetStreamsQuery()

  if (loading) {
    return <div>Loading...</div>
  } else if (error) {
    return <h3 className="error">Error fetching data</h3>
  } else {
    setTimeout(() => {
      props.dispatch({ type: FETCH_STREAMS, payload: data?.allStreams?.nodes })
    })
  }

  return (
    <div>
      {props.streams.map((stream: StreamMessage) => {
        return (
          <div key={stream.id}>
            <h3>{stream.title}</h3>
            <p>{stream.description}</p>
            <small>{stream.createdAt}</small>
          </div>
        )
      })}
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  streams: state.stream,
})

export default connect(mapStateToProps)(StreamList)
