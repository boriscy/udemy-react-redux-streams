import React from "react"
import Button from "components/bulma/Button"
import { useForm, Controller } from "react-hook-form"
import { connect } from "react-redux"
import * as yup from "yup"

import { useCreateStreamMutation } from "generated/graphql"
import { ADD_SNACK } from "actions/types"

const validationSchema = yup.object().shape({
  title: yup.string().required(),
  description: yup
    .string()
    .required()
    .min(10, "Your title must be at least 10 chars length"),
})

const defaultValues = { title: "", description: "" }

const StreamCreate = (props: any) => {
  const { control, handleSubmit, errors, register, reset } = useForm({
    defaultValues,
    validateCriteriaMode: "all",
    validationSchema,
  })
  let saving = false

  //
  const [createStreamMutation] = useCreateStreamMutation()

  const onSubmit = async (formValues: any) => {
    saving = true
    try {
      await createStreamMutation({ variables: formValues })
      props.dispatch({
        type: ADD_SNACK,
        payload: { status: "Success", text: "Stream has been created!" },
      })
      props.history.push("/")
    } catch (e) {
      alert("There was an error saving")
    } finally {
      saving = false
      reset()
    }
  }

  return (
    <div>
      <h2>Stream Create</h2>

      <form onSubmit={handleSubmit(onSubmit)} style={{ maxWidth: "400px" }}>
        <div className="field">
          <Controller
            as={<input />}
            name="title"
            placeholder="Title"
            control={control}
            className={errors.title ? "has-error" : ""}
          />
          <div className="error">{errors.title && errors.title.message}</div>
        </div>

        <div className="field">
          <Controller
            as={<textarea />}
            rows={3}
            name="description"
            placeholder="Description"
            control={control}
            className={errors.description ? "has-error" : ""}
          />
          <div className="error">
            {errors.description && errors.description.message}
          </div>
        </div>
        <Button disabled={saving}>
          {saving ? "Saving..." : "Create Stream"}
        </Button>
      </form>
    </div>
  )
}
/*
const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => ({
  dispatch,
})*/

export default connect()(StreamCreate)
