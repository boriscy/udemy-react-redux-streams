import React from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import LoginRegister from "./LoginRegister"
import { AuthMessage } from "actions/types"

const Header: React.SFC<{ auth: AuthMessage }> = (props) => {
  const { email, name } = props.auth
  const msgIdentifier = name ? name : email

  return (
    <div className="header flex full-width main-menu">
      <Link to="/" className="item">
        <h3>Streamy</h3>
      </Link>
      <div>{msgIdentifier ? `Welcome back ${msgIdentifier}` : ""}</div>
      <div className="right menu text-right">
        <Link to="/" className="item">
          All Streams
        </Link>
        &nbsp;
        <LoginRegister />
        {/*<GoogleAuth /> */}
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  auth: state.auth,
})

export default connect(mapStateToProps)(Header)
