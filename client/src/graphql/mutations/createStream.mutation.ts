import gql from "graphql-tag"

export default gql`
  mutation CreateStream($title: String!, $description: String!) {
    createStream(
      input: { stream: { title: $title, description: $description } }
    ) {
      stream {
        id
        title
        description
        createdAt
      }
    }
  }
`
