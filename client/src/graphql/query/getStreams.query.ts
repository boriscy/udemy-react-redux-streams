import gql from "graphql-tag"

export default gql`
  query getStreams {
    allStreams {
      nodes {
        id
        title
        description
        createdAt
      }
    }
  }
`
