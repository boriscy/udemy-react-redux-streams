import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A location in a connection that can be used for resuming pagination. */
  Cursor: any;
  /** A point in time as described by the [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone. */
  Datetime: any;
};

/** The root query type which gives access points into the data universe. */
export type Query = Node & {
  __typename?: 'Query';
  /** Exposes the root query type nested one level down. This is helpful for Relay 1 which can only query top level fields if they are in a particular form. */
  query: Query;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. */
  nodeId: Scalars['ID'];
  /** Fetches an object given its globally unique `ID`. */
  node?: Maybe<Node>;
  /** Reads and enables pagination through a set of `Stream`. */
  allStreams?: Maybe<StreamsConnection>;
  streamById?: Maybe<Stream>;
  /** Reads a single `Stream` using its globally unique `ID`. */
  stream?: Maybe<Stream>;
};


/** The root query type which gives access points into the data universe. */
export type QueryNodeArgs = {
  nodeId: Scalars['ID'];
};


/** The root query type which gives access points into the data universe. */
export type QueryAllStreamsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<StreamsOrderBy>>;
  condition?: Maybe<StreamCondition>;
};


/** The root query type which gives access points into the data universe. */
export type QueryStreamByIdArgs = {
  id: Scalars['Int'];
};


/** The root query type which gives access points into the data universe. */
export type QueryStreamArgs = {
  nodeId: Scalars['ID'];
};

/** An object with a globally unique `ID`. */
export type Node = {
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
};


/** Methods to use when ordering `Stream`. */
export enum StreamsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  TitleAsc = 'TITLE_ASC',
  TitleDesc = 'TITLE_DESC',
  DescriptionAsc = 'DESCRIPTION_ASC',
  DescriptionDesc = 'DESCRIPTION_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** A condition to be used against `Stream` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type StreamCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `title` field. */
  title?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `description` field. */
  description?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: Maybe<Scalars['Datetime']>;
};


/** A connection to a list of `Stream` values. */
export type StreamsConnection = {
  __typename?: 'StreamsConnection';
  /** A list of `Stream` objects. */
  nodes: Array<Maybe<Stream>>;
  /** A list of edges which contains the `Stream` and cursor to aid in pagination. */
  edges: Array<StreamsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Stream` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type Stream = Node & {
  __typename?: 'Stream';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['Int'];
  title: Scalars['String'];
  description: Scalars['String'];
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
};

/** A `Stream` edge in the connection. */
export type StreamsEdge = {
  __typename?: 'StreamsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Stream` at the end of the edge. */
  node?: Maybe<Stream>;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['Cursor']>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['Cursor']>;
};

/** The root mutation type which contains root level fields which mutate data. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Creates a single `Stream`. */
  createStream?: Maybe<CreateStreamPayload>;
  /** Updates a single `Stream` using its globally unique id and a patch. */
  updateStream?: Maybe<UpdateStreamPayload>;
  /** Updates a single `Stream` using a unique key and a patch. */
  updateStreamById?: Maybe<UpdateStreamPayload>;
  /** Deletes a single `Stream` using its globally unique id. */
  deleteStream?: Maybe<DeleteStreamPayload>;
  /** Deletes a single `Stream` using a unique key. */
  deleteStreamById?: Maybe<DeleteStreamPayload>;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateStreamArgs = {
  input: CreateStreamInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateStreamArgs = {
  input: UpdateStreamInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateStreamByIdArgs = {
  input: UpdateStreamByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteStreamArgs = {
  input: DeleteStreamInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteStreamByIdArgs = {
  input: DeleteStreamByIdInput;
};

/** All input for the create `Stream` mutation. */
export type CreateStreamInput = {
  /** An arbitrary string value with no semantic meaning. Will be included in the payload verbatim. May be used to track mutations by the client. */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Stream` to be created by this mutation. */
  stream: StreamInput;
};

/** An input for mutations affecting `Stream` */
export type StreamInput = {
  id?: Maybe<Scalars['Int']>;
  title: Scalars['String'];
  description: Scalars['String'];
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
};

/** The output of our create `Stream` mutation. */
export type CreateStreamPayload = {
  __typename?: 'CreateStreamPayload';
  /** The exact same `clientMutationId` that was provided in the mutation input, unchanged and unused. May be used by a client to track mutations. */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Stream` that was created by this mutation. */
  stream?: Maybe<Stream>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Stream`. May be used by Relay 1. */
  streamEdge?: Maybe<StreamsEdge>;
};


/** The output of our create `Stream` mutation. */
export type CreateStreamPayloadStreamEdgeArgs = {
  orderBy?: Maybe<Array<StreamsOrderBy>>;
};

/** All input for the `updateStream` mutation. */
export type UpdateStreamInput = {
  /** An arbitrary string value with no semantic meaning. Will be included in the payload verbatim. May be used to track mutations by the client. */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Stream` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Stream` being updated. */
  streamPatch: StreamPatch;
};

/** Represents an update to a `Stream`. Fields that are set will be updated. */
export type StreamPatch = {
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
};

/** The output of our update `Stream` mutation. */
export type UpdateStreamPayload = {
  __typename?: 'UpdateStreamPayload';
  /** The exact same `clientMutationId` that was provided in the mutation input, unchanged and unused. May be used by a client to track mutations. */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Stream` that was updated by this mutation. */
  stream?: Maybe<Stream>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Stream`. May be used by Relay 1. */
  streamEdge?: Maybe<StreamsEdge>;
};


/** The output of our update `Stream` mutation. */
export type UpdateStreamPayloadStreamEdgeArgs = {
  orderBy?: Maybe<Array<StreamsOrderBy>>;
};

/** All input for the `updateStreamById` mutation. */
export type UpdateStreamByIdInput = {
  /** An arbitrary string value with no semantic meaning. Will be included in the payload verbatim. May be used to track mutations by the client. */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Stream` being updated. */
  streamPatch: StreamPatch;
  id: Scalars['Int'];
};

/** All input for the `deleteStream` mutation. */
export type DeleteStreamInput = {
  /** An arbitrary string value with no semantic meaning. Will be included in the payload verbatim. May be used to track mutations by the client. */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Stream` to be deleted. */
  nodeId: Scalars['ID'];
};

/** The output of our delete `Stream` mutation. */
export type DeleteStreamPayload = {
  __typename?: 'DeleteStreamPayload';
  /** The exact same `clientMutationId` that was provided in the mutation input, unchanged and unused. May be used by a client to track mutations. */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Stream` that was deleted by this mutation. */
  stream?: Maybe<Stream>;
  deletedStreamId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Stream`. May be used by Relay 1. */
  streamEdge?: Maybe<StreamsEdge>;
};


/** The output of our delete `Stream` mutation. */
export type DeleteStreamPayloadStreamEdgeArgs = {
  orderBy?: Maybe<Array<StreamsOrderBy>>;
};

/** All input for the `deleteStreamById` mutation. */
export type DeleteStreamByIdInput = {
  /** An arbitrary string value with no semantic meaning. Will be included in the payload verbatim. May be used to track mutations by the client. */
  clientMutationId?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
};

export type CreateStreamMutationVariables = Exact<{
  title: Scalars['String'];
  description: Scalars['String'];
}>;


export type CreateStreamMutation = (
  { __typename?: 'Mutation' }
  & { createStream?: Maybe<(
    { __typename?: 'CreateStreamPayload' }
    & { stream?: Maybe<(
      { __typename?: 'Stream' }
      & Pick<Stream, 'id' | 'title' | 'description' | 'createdAt'>
    )> }
  )> }
);

export type GetStreamsQueryVariables = {};


export type GetStreamsQuery = (
  { __typename?: 'Query' }
  & { allStreams?: Maybe<(
    { __typename?: 'StreamsConnection' }
    & { nodes: Array<Maybe<(
      { __typename?: 'Stream' }
      & Pick<Stream, 'id' | 'title' | 'description' | 'createdAt'>
    )>> }
  )> }
);


export const CreateStreamDocument = gql`
    mutation CreateStream($title: String!, $description: String!) {
  createStream(input: {stream: {title: $title, description: $description}}) {
    stream {
      id
      title
      description
      createdAt
    }
  }
}
    `;
export type CreateStreamMutationFn = ApolloReactCommon.MutationFunction<CreateStreamMutation, CreateStreamMutationVariables>;
export type CreateStreamComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateStreamMutation, CreateStreamMutationVariables>, 'mutation'>;

    export const CreateStreamComponent = (props: CreateStreamComponentProps) => (
      <ApolloReactComponents.Mutation<CreateStreamMutation, CreateStreamMutationVariables> mutation={CreateStreamDocument} {...props} />
    );
    
export type CreateStreamProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<CreateStreamMutation, CreateStreamMutationVariables>
    } & TChildProps;
export function withCreateStream<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CreateStreamMutation,
  CreateStreamMutationVariables,
  CreateStreamProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, CreateStreamMutation, CreateStreamMutationVariables, CreateStreamProps<TChildProps, TDataName>>(CreateStreamDocument, {
      alias: 'createStream',
      ...operationOptions
    });
};

/**
 * __useCreateStreamMutation__
 *
 * To run a mutation, you first call `useCreateStreamMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateStreamMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createStreamMutation, { data, loading, error }] = useCreateStreamMutation({
 *   variables: {
 *      title: // value for 'title'
 *      description: // value for 'description'
 *   },
 * });
 */
export function useCreateStreamMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateStreamMutation, CreateStreamMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateStreamMutation, CreateStreamMutationVariables>(CreateStreamDocument, baseOptions);
      }
export type CreateStreamMutationHookResult = ReturnType<typeof useCreateStreamMutation>;
export type CreateStreamMutationResult = ApolloReactCommon.MutationResult<CreateStreamMutation>;
export type CreateStreamMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateStreamMutation, CreateStreamMutationVariables>;
export const GetStreamsDocument = gql`
    query getStreams {
  allStreams {
    nodes {
      id
      title
      description
      createdAt
    }
  }
}
    `;
export type GetStreamsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetStreamsQuery, GetStreamsQueryVariables>, 'query'>;

    export const GetStreamsComponent = (props: GetStreamsComponentProps) => (
      <ApolloReactComponents.Query<GetStreamsQuery, GetStreamsQueryVariables> query={GetStreamsDocument} {...props} />
    );
    
export type GetStreamsProps<TChildProps = {}, TDataName extends string = 'data'> = {
      [key in TDataName]: ApolloReactHoc.DataValue<GetStreamsQuery, GetStreamsQueryVariables>
    } & TChildProps;
export function withGetStreams<TProps, TChildProps = {}, TDataName extends string = 'data'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  GetStreamsQuery,
  GetStreamsQueryVariables,
  GetStreamsProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withQuery<TProps, GetStreamsQuery, GetStreamsQueryVariables, GetStreamsProps<TChildProps, TDataName>>(GetStreamsDocument, {
      alias: 'getStreams',
      ...operationOptions
    });
};

/**
 * __useGetStreamsQuery__
 *
 * To run a query within a React component, call `useGetStreamsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetStreamsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetStreamsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetStreamsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetStreamsQuery, GetStreamsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetStreamsQuery, GetStreamsQueryVariables>(GetStreamsDocument, baseOptions);
      }
export function useGetStreamsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetStreamsQuery, GetStreamsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetStreamsQuery, GetStreamsQueryVariables>(GetStreamsDocument, baseOptions);
        }
export type GetStreamsQueryHookResult = ReturnType<typeof useGetStreamsQuery>;
export type GetStreamsLazyQueryHookResult = ReturnType<typeof useGetStreamsLazyQuery>;
export type GetStreamsQueryResult = ApolloReactCommon.QueryResult<GetStreamsQuery, GetStreamsQueryVariables>;