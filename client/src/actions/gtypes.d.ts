interface Window {
  gapi: {
    load: Function
    client: {
      init: Function
    }
    auth2: {
      [field: string]: Function
    }
  }
  [field: string]: any
}
// eslint-disable-next-line
let window: Window = global as any
