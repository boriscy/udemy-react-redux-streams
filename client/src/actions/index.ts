import { Dispatch } from "redux"
import {
  SIGN_IN,
  SIGN_OUT,
  AuthMessage,
  CREATE_STREAM,
  FETCH_STREAM,
  DELETE_STREAM,
  EDIT_STREAM,
  FETCH_STREAMS,
} from "./types"
import { useGetStreamsQuery } from "generated/graphql"

export const signIn = ({ userId, email, name }: AuthMessage) => ({
  type: SIGN_IN,
  payload: { userId, email, name },
})

export const signOut = () => ({
  type: SIGN_OUT,
})

export const fetchStreams = () => (dispatch: Dispatch) => {
  const { data, loading, error } = useGetStreamsQuery()
  if (!loading) {
    dispatch({
      type: FETCH_STREAMS,
      payload: data?.allStreams?.nodes,
    })
  }
}
