export const SIGN_IN = "SIGN_IN"
export const SIGN_OUT = "SIGN_OUT"
export const CREATE_STREAM = "CREATE_STREAM"
export const FETCH_STREAMS = "FETCH_STREAMS"
export const FETCH_STREAM = "FETCH_STREAM"
export const DELETE_STREAM = "DELETE_STREAM"
export const EDIT_STREAM = "EDIT_STREAM"
export const ADD_SNACK = "ADD_SNACK"
export const REMOVE_SNACK = "REMOVE_SNACK"

export interface AuthMessage {
  userId: string
  email: string
  name: string
}

export interface StreamMessage {
  id: number
  title: string
  description: string
  createdAt: string
  updatedAt: string
}
